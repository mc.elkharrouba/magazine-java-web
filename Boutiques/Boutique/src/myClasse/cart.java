package myClasse;

import java.util.ArrayList;

public class cart {
	
	private ArrayList<Produit> list;
	
	public cart()
	{
		this.list = new ArrayList<Produit>();
	}

	public void addToCart(Produit p)
	{
		int i = recherche(p.getId());
		
		if( i == -1)
		{
			
			this.list.add(p);
		}
		else
		{
			updateCart(p,1);
		}
	}
	
	public int recherche(int pos) 
	{
		int i=0;
		boolean trouve= false;
		
		while((i< this.list.size()) && (!trouve))
		{
			if( this.list.get(i).getId() == pos  )
			{
				trouve = true;
			}
			else
			{
				i++;
			}
		}
		
		if(trouve)
		{
			return i;
		}
		else
		{
			return -1;
		}
	}
	
	public ArrayList<Produit> showCart()
	{
		return this.list;
	}
	
	public void updateCart(Produit p,int qty)
	{
		int i = recherche(p.getId());
		
		if(i != -1)
		{
			int quantity = this.list.get(i).getQuantite();
			this.list.get(i).setQuantite(quantity+qty);
		}
	}
	
	public float toatlCart()
	{
		float total=0;
		
		for (Produit produit : list) {
			total = total + ( produit.getPrice()  * produit.getQuantite());
		}
		
		return total;
	}
	
	public void clearCart()
	{
		this.list.clear();
	}
	
}
