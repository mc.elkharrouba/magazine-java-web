package myClasse;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;



public class GateWay {

	public static Connection connection()
	{
		Connection conn = null;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost/boutique";
			conn = DriverManager.getConnection(url, "root", "");
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return conn;
	}
	
	
	public static int delete(int id) 
	{
		int num = 0;
		try {
			
			Connection conn = GateWay.connection();
			String sql = "delete from category where id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			num = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return num;
	}
	
	public static Produit viewProduit( int id )
	{
		Produit p = new Produit();
		
		Connection conn = GateWay.connection();
	
		
		try {
			String sql = "SELECT * FROM `produit` WHERE id= ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				p.setId(rs.getInt(1));
				p.setTitle(rs.getString(2));
				p.setDescription(rs.getString(3));
				p.setImg(rs.getString(4));
				p.setPrice(rs.getFloat(5));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return p;
	}
	
	public static categorie viewCategorie( int id )
	{
		categorie cat = new categorie();
		
		Connection conn = GateWay.connection();
	
		
		try {
			String sql = "SELECT * FROM `category` WHERE id= ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				cat.setId(rs.getInt(1));
				cat.setTitle(rs.getString(2));
				cat.setImg(rs.getString(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return cat;
	}
	
	public static ArrayList<categorie> viewCategories()
	{
		ArrayList<categorie> list = new ArrayList<categorie>();
		
		Connection conn = GateWay.connection();
		
		
		try {
			
			String sql = "SELECT * FROM `category`";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			
			ResultSet rs = ps.executeQuery();
			while (rs.next())
			{
				categorie cat = new categorie();
				cat.setId(rs.getInt(1));
				cat.setTitle(rs.getString(2));
				cat.setImg(rs.getString(3));
				list.add(cat);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return list;
	}
	

	
	public static ArrayList<Produit> viewProduits(int id)
	{
		
		ArrayList<Produit> list = new ArrayList<Produit>();
		Connection conn = GateWay.connection();
		
		try {
			String sql = "select * from produit where idc = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{
				Produit p = new Produit();
				p.setId(rs.getInt(1));
				p.setTitle(rs.getString(2));
				p.setDescription(rs.getString(3));
				p.setImg(rs.getString(4));
				p.setPrice(rs.getFloat(5));
				
				list.add(p);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return list ;
	}
	
}
