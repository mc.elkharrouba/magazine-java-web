package myClasse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ServletAcceuil
 */
@WebServlet("/ServletAcceuil")
public class ServletAcceuil extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAcceuil() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		PrintWriter print = response.getWriter();
		if(request.getParameter("id") == null)
		{
			response.sendRedirect("http://localhost:8081/Boutique/index.jsp");
		}
		else 
		{
			HttpSession session = request.getSession();
			
			String idc = request.getParameter("id");
			int id = Integer.parseInt(idc);
			session.setAttribute("idproduct", id);
			
			categorie cat =  GateWay.viewCategorie(id);
			request.setAttribute("categorieId", cat);
			
			ArrayList<Produit> listProduit = GateWay.viewProduits(id);
			request.setAttribute("listByProduit", listProduit);
			
			ArrayList<categorie> list = GateWay.viewCategories();
			request.setAttribute("categorieList", list);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/categorie.jsp");
			dispatcher.forward(request, response);
			
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
	
	}

}
