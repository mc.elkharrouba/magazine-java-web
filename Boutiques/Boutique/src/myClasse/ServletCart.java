package myClasse;

import java.io.IOException;
import java.io.PipedWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ServletCart
 */
@WebServlet("/ServletCart")
public class ServletCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
    protected void clear(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
    	HttpSession session = request.getSession();
    	
    	if(session.getAttribute("cartSession") == null)
		{
			response.sendRedirect("http://localhost:8081/Boutique/index.jsp");
		}
		else 
		{
			cart Cart = (cart) session.getAttribute("cartSession") ;
			Cart.clearCart();
			
			session.removeAttribute("cartSession");
			//request.setAttribute("cartPage", list);
			session.removeAttribute("totalCart");
			RequestDispatcher disptach = request.getRequestDispatcher("/index.jsp");
			disptach.forward(request, response);
			
			
		}
    	
	}
    
    protected void browse(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		
			
		if(session.getAttribute("idproduct") == null)
		{
			response.sendRedirect("http://localhost:8081/Boutique/index.jsp");
		}
		else 
		{
		
			int id = (int) session.getAttribute("idproduct") ;
		
		
			RequestDispatcher disptach = request.getRequestDispatcher("ServletAcceuil?id="+id);
			disptach.forward(request, response);
			
			
		}
		
	}
    
	protected void addToCart(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		
			
		if(session.getAttribute("cartSession") == null)
		{
			response.sendRedirect("http://localhost:8081/Boutique/index.jsp");
		}
		else 
		{
			cart Cart = (cart) session.getAttribute("cartSession") ;
			ArrayList<Produit> list = Cart.showCart();
			request.setAttribute("cartPage", list);
			session.setAttribute("totalCart", Cart.toatlCart());
			RequestDispatcher disptach = request.getRequestDispatcher("/cart.jsp");
			disptach.forward(request, response);
			
			
		}
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		
		String action = request.getParameter("action");
		PrintWriter print = response.getWriter();
		if(action == null)
		{
			action = "addToCart";
		}
		
		switch (action) {
		case "addToCart": 
			
			addToCart(request, response);
			break;
		
		case "clear":
		
			clear(request, response);
			break;
			
		case "browse":
			
			browse(request, response);
			break;
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
