package myClasse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet implementation class ServletCategorie
 */
@WebServlet("/ServletCategorie")
public class ServletCategorie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCategorie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
			
				
		
				PrintWriter print = response.getWriter();
				HttpSession session = request.getSession();
				//int id = (int) session.getAttribute("idproduct");
				String idproduct = request.getParameter("newId");
				int idc= Integer.parseInt(idproduct);
				
				if( session.getAttribute("cartSession") == null) {
				
					cart c = new cart();
					Produit p = GateWay.viewProduit(idc);
					c.addToCart(p);
					
					session.setAttribute("cartSession", c);
					
					
					//ArrayList<Produit> listCart =  c.showCart();
					
					
					//session.setAttribute("QtyCart", listCart.size());
					//response.setContentType("text/html");
					//print.println(listCart.size());
					
					int qtyInCart = 1;
					
					session.setAttribute("QtyCart", qtyInCart);
					response.setContentType("text/html");
					print.println(qtyInCart);
					System.out.print(qtyInCart);
					
				}
				else 
				{
					cart c = (cart) session.getAttribute("cartSession");
					Produit p = GateWay.viewProduit(idc);
					c.addToCart(p);
					
					session.setAttribute("cartSession", c);
					ArrayList<Produit> listCart = c.showCart();
					
					
					//session.setAttribute("QtyCart", listCart.size());
					//response.setContentType("text/html");
					//print.println(listCart.size());
					
					int qtyInCart = (int) session.getAttribute("QtyCart") + 1;
					
					session.setAttribute("QtyCart", qtyInCart);
					response.setContentType("text/html");
					print.println(qtyInCart);
					System.out.print(qtyInCart);
					
			
						
				}
					
			}
				
				
				
				
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

}
