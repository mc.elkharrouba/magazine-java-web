<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
</!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
	 <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/mini.js"></script>
</head>
<body>
						<%
							int cartQty = 0;
							
							if( session.getAttribute("cartSession") != null ){
							
								cartQty = (int) session.getAttribute("QtyCart");
							}
						%>

	
			<div id="header">
							<div class="upper-nav">
								<div class="container">
									<div class="row">
									    <div class="col-sm text-sm-left">
									       <i class="fas fa-phone-alt">&nbsp;</i><span>(432)782-1820</span>&nbsp;&nbsp;&nbsp;
									       <i class="far fa-envelope"></i>&nbsp;<span>mc.elkharrouba@gmail.com</span>
									    </div>
									    <div class="col-sm sm text-sm-right text-right">
									      <span>Sign Up</span>&nbsp;&nbsp;&nbsp;
									      <span class="signin">Sign in</span>
									    </div>
									</div>
								</div>
							</div>
						<div class="second-nav">
								<div class="container">
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="#">Food<span class="gadget-logo">MA</span></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									    <ul class="navbar-nav ml-auto">
									      <li class="nav-item active">
									        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">shop</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">about us</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">contact us</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">
									        	<i class="fas fa-shopping-cart cart">
									        	<span class="badge badge-light"><%=cartQty %></span></i>
									        </a>
									      </li>
									    </ul>
									  </div>
									</nav>
								</div>
						</div>
				</div>
				<div id="content">
					<div class="slider">
						 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						 
							  <div class="carousel-inner">
							
							  	<h1 class="text-center">Profitez de notre promotion d'été...</h1>
							  	<div class="carousel-overly"></div>
							    <div class="carousel-item slider-one active" style="background-image: url('./img/back-cookies.jpg');">
							     
							    </div>
							    <div class="carousel-item slider-two" style="background-image: url('./img/back-food.jpg
							   xz');">
							      
							    </div>
							    <div class="carousel-item slider-three">
							      
							    </div>
							  </div>
							 
							 
							    <ol class="carousel-indicators">
							    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							  </ol>
							</div>
					</div>
					<div class="section">
						<div class="container">
								<div class="text text-center">
									<h1>Category</h1>
								</div>
							<div class="row mx-md-n5">
								<div class="col-sm-12 col-md-6 col-lg-3 ">
									<a href="http://localhost:8081/Boutique/ServletAcceuil?id=7">
										<div class="flex">
											<span class="Link">Dairy</span>
											<div class="col-overly rounded media-photo"></div>
											<img src="img/1pic.jpg" class="rounded mx-auto d-block media-photo" alt="Dairy">
										</div>
									</a>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3  ">
									 <a href="http://localhost:8081/Boutique/ServletAcceuil?id=3">
										<div class="flex">
											<span class="Link">Meats</span>
											<div class="col-overly rounded media-photo"></div>
											<img src="img/2pic.jpg"  class="rounded mx-auto d-block media-photo" alt="...">
										</div>
									</a>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3 ">
									<a href="http://localhost:8081/Boutique/ServletAcceuil?id=5">
										<div class="flex">
											<span class="Link">Bakery</span>
											<div class="col-overly rounded media-photo"></div>
											<img src="img/3pic.jpg"  class="rounded mx-auto d-block media-photo" alt="...">
										</div>
									</a>
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3 ">
								  <a href="http://localhost:8081/Boutique/ServletAcceuil?id=2">
										<div class="flex">
											<span class="Link">fruits & veg</span>
											<div class="col-overly rounded media-photo"></div>
											<img src="img/4pic.jpg" class="rounded mx-auto d-block media-photo" alt="...">
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="footer">
						<div class="container">
							<div class="row">
								<div class="col-sm-12 col-md-6 col-lg-3 ">
									<h4>EN LIGNE</h4>
									<p>Blog</p>
									<p>video </p>
									<p>Application mobile</p>
									<p>Application tablette </p>
									
								</div>
								<div class="col-sm-12 col-md-6 col-lg-3 ">
									<h4>PROMOTIONS</h4>
									<p>Discount </p>
									<p>FinalGadget GiftCard</p>
								</div>
							
								<div class="col-sm-12 col-md-6 col-lg-6">
									<h4>CONNECT WITH US</h4>
									<div class="marign-top">
										<i class="fab fa-facebook fa-2x"></i>&nbsp;&nbsp;&nbsp;
										<i class="fab fa-instagram fa-2x"></i>&nbsp;&nbsp;&nbsp;
										<i class="fab fa-twitter fa-2x"></i>&nbsp;&nbsp;&nbsp;
										<i class="fab fa-pinterest fa-2x"></i>
									</div> 
									<h4 class="marign-top">NEWSLETTER: SIGN UP & GET 10% OFF!</h4>
								
									<div class="input-group mb-3 flex-input">
									  <input type="text" class="form-control flex-input-border" placeholder="Email adresse" aria-label="Recipient's username" aria-describedby="button-addon2">
									  <div class="input-group-append">
									    <button class="btn btn-outline-secondary flex-input-border" type="button" id="button-addon2" >Button</button>
									  </div>
									</div>
									
								</div>
							</div>
						</div>
					
					</div>
				</div>
				
							

							

	

	
	 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="static/min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="js/mini.js"></script>
</body>
</html>