<%@page import="myClasse.cart"%>
<%@page import="myClasse.Produit"%>
<%@page import="myClasse.categorie"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
</!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/mini.js"></script>

				<script src="https://code.jquery.com/jquery-1.10.2.js"
	type="text/javascript"></script>
			<script type="text/javascript">
	
	function addToCart(id){
		$(document).ready(function() {
			
				$.ajax({
					url : 'ServletCategorie',
					data :{
						newId : id
					},
					success : function(responseText) {
						$('#qtyBadge').text(responseText);
					}
				});
		
		});
			}
    </script>
    
</head>
<body>
							<%			
								int cartQty = 0;
								
								if( session.getAttribute("cartSession") != null ){
								
									cartQty = (int) session.getAttribute("QtyCart");
									
								}

							%>
				<div id="header">
							<div class="upper-nav">
								<div class="container">
									<div class="row">
									    <div class="col-sm">
									       <i class="fas fa-phone-alt">&nbsp;</i><span>(432)782-1820</span>&nbsp;&nbsp;&nbsp;
									       <i class="far fa-envelope"></i>&nbsp;<span>mc.elkharrouba@gmail.com</span>
									    </div>
									    <div class="col-sm text-right">
									      <span>Sign Up</span>&nbsp;&nbsp;&nbsp;
									      <span class="signin">Sign in</span>
									    </div>
									</div>
								</div>
							</div>
						<div class="second-nav">
								<div class="container">
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="#">Food<span class="gadget-logo">MA</span></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									    <ul class="navbar-nav ml-auto">
									      <li class="nav-item ">
									        <a class="nav-link" href="#">Home</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">shop</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">about us</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">contact us</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">
									        	<i class="fas fa-shopping-cart cart">
									        	<span class="badge badge-light" id="qtyBadge"><%=cartQty %></span></i>
									        </a>
									      </li>
									    </ul>
									  </div>
									</nav>
								</div>
						</div>
				</div>
				<div id="content">
				
				</div>
				

								<section>
									<%
										ArrayList<categorie> list = (ArrayList<categorie>) request.getAttribute("categorieList");
										categorie  cat = (categorie)  request.getAttribute("categorieId");
										ArrayList<Produit> listP = (ArrayList<Produit>) request.getAttribute("listByProduit");
									%>
									<h3 class="text-center" style="margin-top: 60px;font-size: 39px;">Products</h3>
									
		
									
										<br/>
										

	<div class="container-fluid">
                                        <div class="row">
                                                      <div class="col-md-3 col-lg-3 ">
                                                              <div class="container">
                                                                      <h4 class="text-center sidebar-left-title">CATEGORY</h4>
                                                                      
                                                                      <div class="nav flex-column nav-pills text-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                                     
                                                                        <% 
									  		for(categorie mylist : list)
									  		{ 
									  			if(mylist.getId() == cat.getId())
									  			{
									  				
									  				out.print("<a class='nav-link active' href='http://localhost:8081/Boutique/ServletAcceuil?id="+mylist.getId()+"' role='tab' aria-controls='v-pills-home' aria-selected='true' >"+mylist.getTitle()+"</a>");
									  			}
									  			else
									  			{
									  				out.print("<a class='nav-link' href='http://localhost:8081/Boutique/ServletAcceuil?id="+mylist.getId()+"' role='tab' aria-controls='v-pills-home' aria-selected='true' >"+mylist.getTitle()+"</a>");
									  			}

									  			
					
									  		}
									  	%>
                                                                      </div>
                                                              </div>
                                                      </div>
                                                      <div class="col-md-9 col-lg-9 ">
                                                      
                                                      
                                                      <div class="container">

  			
								
								<table id="productsTable"  class="table table-condensed table-bordered">
												
									<thead>					
										<tr>					
											<th>Id</th>
											<th>&#160;</th>
											<th>nom</th>
											<th>Description</th>
											<th>Unit Price</th>			
											<th>Purchase</th>
									
										</tr>					
									</thead>
									<% for(Produit p : listP){ %>
										<tr>
										<td><%=p.getId() %></td>
										<td>
											<img alt="" height="100px" width="100px" src="./img/<%=p.getImg() %>">
										</td>
										<td>
											<%=p.getTitle() %> 
										</td>
										<td>
											<%=p.getDescription() %>
										</td>
										<td>
											<%=p.getPrice() %>
										</td>
										
										<td>
											<button type="button" class="btn btn-warning" onclick="addToCart(<%=p.getId()%>)">Purchase</button>
										</td>
									
										</tr>
									<%} %>
									 <tfoot>
										<tr>					
											<th>Id</th>
											<th>&#160;</th>
											<th>Name</th>
											<th>Description</th>
											<th>Unit Price</th>				
											<th>Purchase</th>
										
										</tr>									
									</tfoot>
									
									<!-- Modal -->
							
									
									
												
								</table>
		
		


			</div>
                                                                
                                                      </div>
                           				 </div>
    </div>

									<div id="badge">
										<div class="alert alert-success" role="alert">
											<div id="btn-left">
											 <a type="button" class="btn btn-success" role="button" href="http://localhost:8081/Boutique/ServletCart">view Cart!</a>
											 <% 
											 int cnt=1;
											 if(session.getAttribute("cartSession") != null) {
												  	    	cart Cart = (cart) session.getAttribute("cartSession") ;
												        	ArrayList<Produit> cartPay = Cart.showCart();
											 
											 %>
											     	<form name="paypalForm" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
																			<input type="hidden" name="cmd" value="_cart" />
																			<input type="hidden" name="upload" value="1" />
																			<input type="hidden" name="business" value="info.elkharrouba@gmail.com" />

																			<% for(Produit p : cartPay) {%>
																			<input type="hidden" name="item_name_<%=cnt %>" value="<%=p.getTitle() %> " >
																			<input type="hidden" name="amount_<%=cnt %>" value="<%=p.getPrice()%>">
																			<input type="hidden" name="quantity_<%=cnt %>" value="<%=p.getQuantite()%>">
																			<%cnt++;} %>
																			<input type="hidden" name="currency_code" value="USD"> 
																			<input type="hidden" name="return" value="http://localhost:8080/PaypalGS/paypalResponse.jsp" />
																			<input type="hidden" name="cancel_return" value="http://localhost:8080/PaypalGS/paypalResponseCancel.jsp" />
																			<input class="btn btn-success" type="submit" value="PROCEED TO CHECK OUT">
																			</form>
											<%
											 	}
											%>								
 											</div>
										</div>
									</div>
									
								</section>
		
</body>
</html>