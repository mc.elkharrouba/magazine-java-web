<%@page import="myClasse.GateWay"%>
<%@page import="myClasse.Produit"%>
<%@page import="java.util.ArrayList"%>
<%@page import="myClasse.cart"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
</!DOCTYPE html>
<html>
<head>
	<title>Boutique</title>
	<title>Boutique</title>
	  	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
    <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="js/mini.js"></script>

</head>
<body>
	
		<div class="container-fluid">
				<!-- Image and text -->
				
							<div class="upper-nav">
								<div class="container">
									<div class="row">
									    <div class="col-sm text-sm-left">
									       <i class="fas fa-phone-alt">&nbsp;</i><span>(432)782-1820</span>&nbsp;&nbsp;&nbsp;
									       <i class="far fa-envelope"></i>&nbsp;<span>mc.elkharrouba@gmail.com</span>
									    </div>
									    <div class="col-sm sm text-sm-right text-right">
									      <span>Sign Up</span>&nbsp;&nbsp;&nbsp;
									      <span class="signin">Sign in</span>
									    </div>
									</div>
								</div>
							</div>
						<div class="second-nav">
							
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="#">Food<span class="gadget-logo">MA</span></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									    <ul class="navbar-nav ml-auto">
									      <li class="nav-item ">
									        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">shop</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">about us</a>
									      </li>
									      <li class="nav-item">
									        <a class="nav-link" href="#">contact us</a>
									      </li>
									    </ul>
									  </div>
									</nav>
						</div>
						
				</div>
						<%
							int cnt= 1;
							ArrayList<Produit> list  = (ArrayList<Produit>) request.getAttribute("cartPage");
							
	
						%>
					
								
									   <section class="shopping-cart spad">
										        <div class="container">
										            <div class="row">
										                <div class="col-lg-12">
										                    <div class="cart-table">
										                        <table>
										                            <thead>
										                                <tr>
										                                    <th>Image</th>
										                                    <th class="p-name">Product Name</th>
										                                    <th>Price</th>
										                                    <th>Quantity</th>
										                                    <th>Total</th>
										                                    <th><i class="ti-close"></i></th>
										                                </tr>
										                            </thead>
										                            <tbody>
										                                
										                                <% for(Produit p : list){ %>
										                                <tr>
										                                    <td class="cart-pic first-row"><img src="img/<%=p.getImg() %>" height="80" width="80"  alt=""></td>
										                                    <td class="cart-title first-row">
										                                        <h5><%=p.getTitle() %></h5>
										                                    </td>
										                                    <td class="p-price first-row">$<%=p.getPrice() %></td>
										                                    <td class="qua-col first-row">
										                                        <div class="quantity">
										                                            <div class="pro-qty">
										                                                <input type="text" value="<%=p.getQuantite()%>">
										                                            </div>
										                                        </div>
										                                    </td>
										                                    <td class="total-price first-row"><%=(p.getPrice() * p.getQuantite()) %></td>
										                                    <td class="close-td first-row"><i class="ti-close"></i></td>
										                                </tr>
										                                <%} %>
										                            </tbody>
										                        </table>
										                    </div>
										                    <div class="row">
										                        <div class="col-lg-4">
										                            <div class="cart-buttons">
										                                <a href="http://localhost:8081/Boutique/ServletCart?action=clear" class="primary-btn continue-shop">Clear cart!</a>
										                                <a href="http://localhost:8081/Boutique/ServletCart?action=browse" class="primary-btn up-cart">Continue shopping!</a>
										                            </div>
										                         
										                        </div>
										                        <div class="col-lg-4 offset-lg-4">
										                            <div class="proceed-checkout">
										                                <ul>
										                                    <li class="subtotal">Subtotal <span>$<%=session.getAttribute("totalCart") %></span></li>
										                                    <li class="cart-total">Total <span>$<%=session.getAttribute("totalCart") %></span></li>
										                                </ul>
										                                
		
										                                	<form name="paypalForm" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
																			<input type="hidden" name="cmd" value="_cart" />
																			<input type="hidden" name="upload" value="1" />
																			<input type="hidden" name="business" value="info.elkharrouba@gmail.com" />
																			
																			<% for(Produit p : list) {%>
																			<input type="hidden" name="item_name_<%=cnt %>" value="<%=p.getTitle() %> " >
																			<input type="hidden" name="amount_<%=cnt %>" value="<%=p.getPrice()%>">
																			<input type="hidden" name="quantity_<%=cnt %>" value="<%=p.getQuantite()%>">
																			<%cnt++;} %>
																			<input type="hidden" name="currency_code" value="USD"> 
																			<input type="hidden" name="return" value="http://localhost:8080/PaypalGS/paypalResponse.jsp" />
																			<input type="hidden" name="cancel_return" value="http://localhost:8080/PaypalGS/paypalResponseCancel.jsp" />
																			<input class="proceed-btn" type="submit" value="PROCEED TO CHECK OUT">
																			</form>
										                            
										                                
										                            </div>
										                        </div>
										                    </div>
										                </div>
										            </div>
										        </div>

								</section>




				     
				  
				
  
		</div>
	</div>
</body>
</html>